import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { ChartsComponent } from './charts/charts.component';
import { BattingStatsComponent } from './batting-stats/batting-stats.component';
import { BowlingStatsComponent } from './batting-stats/bowling-stats.component';
import { BattingStatsPositionComponent } from './batting-stats/batting-stats-position.component';
import { PlayerStatsAllTeamsComponent } from './batting-stats/players-stats-allteams.component';
import { BattingStatsPartnershipsComponent } from './batting-stats/batting-stats-partnerships.component';
import { NgChartsModule } from 'ng2-charts';
import { GuiGridModule } from '@generic-ui/ngx-grid';


@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    ChartsComponent,
    BattingStatsComponent,
    BattingStatsPositionComponent,
    BowlingStatsComponent,
    PlayerStatsAllTeamsComponent,
    BattingStatsPartnershipsComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    NgChartsModule,
    GuiGridModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      //{ path: 'counter', component: CounterComponent },
      //{ path: 'fetch-data', component: FetchDataComponent },
      { path: 'charts', component: ChartsComponent },
      { path: 'batting-stats', component: BattingStatsComponent },
      { path: 'batting-stats-position', component: BattingStatsPositionComponent },
      { path: 'bowling-stats', component: BowlingStatsComponent },
      { path: 'players-stats-allteams', component: PlayerStatsAllTeamsComponent },
      { path: 'batting-stats-partnerships', component: BattingStatsPartnershipsComponent },
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
