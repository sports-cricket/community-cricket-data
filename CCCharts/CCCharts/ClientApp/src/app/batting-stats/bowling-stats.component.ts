import { Component } from '@angular/core';

import { GuiColumn, GuiDataType, GuiSorting } from '@generic-ui/ngx-grid';

//import * as data from "./OverallBattingPerformance.json";

declare var require: any

@Component({
  selector: 'app-bowling-stats-component',
  templateUrl: './bowling-stats.component.html',
  styleUrls: ['./bowling-stats.component.css']
})

export class BowlingStatsComponent {
  data = `[
  {
    "Name": "Farhad Mahbub",
    "TotalInnings": 27,
    "TotalWickets": 29,
    "TotalOvers": 99.1,
    "MaidenOvers": 2,
    "Runs": 572,
    "Economy": 5.77,
    "Average": 19.72,
    "StrikeRate": 20.52,
    "DotballPercentage": 53.61,
    "WidesPercentage": 6.39,
    "NoBallPercentage": 0.34,
    "BoundaryPercentage": 9.75
  },
  {
    "Name": "Suzon Parvez",
    "TotalInnings": 21,
    "TotalWickets": 23,
    "TotalOvers": 83.1,
    "MaidenOvers": 6,
    "Runs": 413,
    "Economy": 4.97,
    "Average": 17.96,
    "StrikeRate": 21.7,
    "DotballPercentage": 63.13,
    "WidesPercentage": 11.42,
    "NoBallPercentage": 1.4,
    "BoundaryPercentage": 7.41
  },
  {
    "Name": "Md Shariful Islam",
    "TotalInnings": 22,
    "TotalWickets": 19,
    "TotalOvers": 52.2,
    "MaidenOvers": 0,
    "Runs": 295,
    "Economy": 5.64,
    "Average": 15.53,
    "StrikeRate": 16.53,
    "DotballPercentage": 45.54,
    "WidesPercentage": 7.01,
    "NoBallPercentage": 0.32,
    "BoundaryPercentage": 6.37
  },
  {
    "Name": "Skd Rahul",
    "TotalInnings": 24,
    "TotalWickets": 19,
    "TotalOvers": 71.5,
    "MaidenOvers": 1,
    "Runs": 467,
    "Economy": 6.5,
    "Average": 24.58,
    "StrikeRate": 22.68,
    "DotballPercentage": 47.1,
    "WidesPercentage": 7.19,
    "NoBallPercentage": 0.93,
    "BoundaryPercentage": 9.28
  },
  {
    "Name": "Khan Rahamatullah",
    "TotalInnings": 26,
    "TotalWickets": 18,
    "TotalOvers": 63.1,
    "MaidenOvers": 2,
    "Runs": 390,
    "Economy": 6.17,
    "Average": 21.67,
    "StrikeRate": 21.06,
    "DotballPercentage": 43.27,
    "WidesPercentage": 6.86,
    "NoBallPercentage": 0.0,
    "BoundaryPercentage": 7.65
  },
  {
    "Name": "Himel Barman",
    "TotalInnings": 20,
    "TotalWickets": 17,
    "TotalOvers": 62.0,
    "MaidenOvers": 3,
    "Runs": 383,
    "Economy": 6.18,
    "Average": 22.53,
    "StrikeRate": 21.88,
    "DotballPercentage": 52.42,
    "WidesPercentage": 14.52,
    "NoBallPercentage": 1.61,
    "BoundaryPercentage": 9.14
  },
  {
    "Name": "Rakesh Mondal",
    "TotalInnings": 20,
    "TotalWickets": 11,
    "TotalOvers": 45.3,
    "MaidenOvers": 0,
    "Runs": 298,
    "Economy": 6.55,
    "Average": 27.09,
    "StrikeRate": 24.82,
    "DotballPercentage": 44.69,
    "WidesPercentage": 9.89,
    "NoBallPercentage": 2.2,
    "BoundaryPercentage": 7.69
  },
  {
    "Name": "Muhitur Shuvo",
    "TotalInnings": 12,
    "TotalWickets": 8,
    "TotalOvers": 25.0,
    "MaidenOvers": 0,
    "Runs": 163,
    "Economy": 6.52,
    "Average": 20.38,
    "StrikeRate": 18.75,
    "DotballPercentage": 42.0,
    "WidesPercentage": 14.0,
    "NoBallPercentage": 0.67,
    "BoundaryPercentage": 6.67
  },
  {
    "Name": "Agnik Podder",
    "TotalInnings": 3,
    "TotalWickets": 5,
    "TotalOvers": 12.1,
    "MaidenOvers": 1,
    "Runs": 44,
    "Economy": 3.62,
    "Average": 8.8,
    "StrikeRate": 14.6,
    "DotballPercentage": 58.9,
    "WidesPercentage": 8.22,
    "NoBallPercentage": 0.0,
    "BoundaryPercentage": 1.37
  },
  {
    "Name": "Shoybul Shakib",
    "TotalInnings": 8,
    "TotalWickets": 4,
    "TotalOvers": 16.1,
    "MaidenOvers": 0,
    "Runs": 87,
    "Economy": 5.38,
    "Average": 21.75,
    "StrikeRate": 24.25,
    "DotballPercentage": 49.48,
    "WidesPercentage": 10.31,
    "NoBallPercentage": 0.0,
    "BoundaryPercentage": 5.15
  },
  {
    "Name": "Shaymal Paul Jackey",
    "TotalInnings": 9,
    "TotalWickets": 3,
    "TotalOvers": 17.1,
    "MaidenOvers": 0,
    "Runs": 137,
    "Economy": 7.98,
    "Average": 45.67,
    "StrikeRate": 34.33,
    "DotballPercentage": 44.66,
    "WidesPercentage": 28.16,
    "NoBallPercentage": 4.85,
    "BoundaryPercentage": 9.71
  },
  {
    "Name": "Abu Jahed Chowdhury",
    "TotalInnings": 6,
    "TotalWickets": 3,
    "TotalOvers": 10.0,
    "MaidenOvers": 0,
    "Runs": 64,
    "Economy": 6.4,
    "Average": 21.33,
    "StrikeRate": 20.0,
    "DotballPercentage": 33.33,
    "WidesPercentage": 5.0,
    "NoBallPercentage": 0.0,
    "BoundaryPercentage": 8.33
  },
  {
    "Name": "Saikat Barua",
    "TotalInnings": 6,
    "TotalWickets": 2,
    "TotalOvers": 11.3,
    "MaidenOvers": 0,
    "Runs": 68,
    "Economy": 5.91,
    "Average": 34.0,
    "StrikeRate": 34.5,
    "DotballPercentage": 44.93,
    "WidesPercentage": 7.25,
    "NoBallPercentage": 1.45,
    "BoundaryPercentage": 5.8
  },
  {
    "Name": "Rokonuzzaman Tuhin",
    "TotalInnings": 5,
    "TotalWickets": 2,
    "TotalOvers": 7.0,
    "MaidenOvers": 0,
    "Runs": 61,
    "Economy": 8.71,
    "Average": 30.5,
    "StrikeRate": 21.0,
    "DotballPercentage": 45.24,
    "WidesPercentage": 19.05,
    "NoBallPercentage": 4.76,
    "BoundaryPercentage": 21.43
  },
  {
    "Name": "Adrian Sharafi",
    "TotalInnings": 3,
    "TotalWickets": 2,
    "TotalOvers": 6.0,
    "MaidenOvers": 0,
    "Runs": 37,
    "Economy": 6.17,
    "Average": 18.5,
    "StrikeRate": 18.0,
    "DotballPercentage": 33.33,
    "WidesPercentage": 5.56,
    "NoBallPercentage": 0.0,
    "BoundaryPercentage": 5.56
  },
  {
    "Name": "Khonodoker Ahmed Shaon",
    "TotalInnings": 6,
    "TotalWickets": 1,
    "TotalOvers": 16.0,
    "MaidenOvers": 0,
    "Runs": 85,
    "Economy": 5.31,
    "Average": 85.0,
    "StrikeRate": 96.0,
    "DotballPercentage": 40.62,
    "WidesPercentage": 1.04,
    "NoBallPercentage": 2.08,
    "BoundaryPercentage": 4.17
  },
  {
    "Name": "Jubayer Ibna Kalam",
    "TotalInnings": 2,
    "TotalWickets": 1,
    "TotalOvers": 3.0,
    "MaidenOvers": 0,
    "Runs": 23,
    "Economy": 7.67,
    "Average": 23.0,
    "StrikeRate": 18.0,
    "DotballPercentage": 61.11,
    "WidesPercentage": 27.78,
    "NoBallPercentage": 5.56,
    "BoundaryPercentage": 11.11
  },
  {
    "Name": "Toslim Arif",
    "TotalInnings": 2,
    "TotalWickets": 0,
    "TotalOvers": 3.0,
    "MaidenOvers": 0,
    "Runs": 25,
    "Economy": 8.33,
    "Average": "Infinity",
    "StrikeRate": "Infinity",
    "DotballPercentage": 50.0,
    "WidesPercentage": 44.44,
    "NoBallPercentage": 0.0,
    "BoundaryPercentage": 11.11
  },
  {
    "Name": "Amlan Karmakar",
    "TotalInnings": 3,
    "TotalWickets": 0,
    "TotalOvers": 6.0,
    "MaidenOvers": 0,
    "Runs": 51,
    "Economy": 8.5,
    "Average": "Infinity",
    "StrikeRate": "Infinity",
    "DotballPercentage": 30.56,
    "WidesPercentage": 27.78,
    "NoBallPercentage": 5.56,
    "BoundaryPercentage": 5.56
  },
  {
    "Name": "Nazmul Mithun",
    "TotalInnings": 1,
    "TotalWickets": 0,
    "TotalOvers": 0.5,
    "MaidenOvers": 0,
    "Runs": 9,
    "Economy": 10.8,
    "Average": "Infinity",
    "StrikeRate": "Infinity",
    "DotballPercentage": 40.0,
    "WidesPercentage": 60.0,
    "NoBallPercentage": 40.0,
    "BoundaryPercentage": 0.0
  },
  {
    "Name": "Md Habibur Ra",
    "TotalInnings": 1,
    "TotalWickets": 0,
    "TotalOvers": 2.0,
    "MaidenOvers": 0,
    "Runs": 15,
    "Economy": 7.5,
    "Average": "Infinity",
    "StrikeRate": "Infinity",
    "DotballPercentage": 75.0,
    "WidesPercentage": 91.67,
    "NoBallPercentage": 0.0,
    "BoundaryPercentage": 0.0
  },
  {
    "Name": "Salman Anwar",
    "TotalInnings": 1,
    "TotalWickets": 0,
    "TotalOvers": 1.0,
    "MaidenOvers": 0,
    "Runs": 16,
    "Economy": 16.0,
    "Average": "Infinity",
    "StrikeRate": "Infinity",
    "DotballPercentage": 0.0,
    "WidesPercentage": 33.33,
    "NoBallPercentage": 0.0,
    "BoundaryPercentage": 33.33
  }
]`

  dataT = `[
  {
    "Name": "Farhad Mahbub",
    "TotalInnings": 16,
    "TotalWickets": 22,
    "TotalOvers": 59.1,
    "MaidenOvers": 2,
    "Runs": 304,
    "Economy": 5.14,
    "Average": 13.82,
    "StrikeRate": 16.14,
    "DotballPercentage": 55.77,
    "WidesPercentage": 3.94,
    "NoBallPercentage": 0.56,
    "BoundaryPercentage": 7.61
  },
  {
    "Name": "Suzon Parvez",
    "TotalInnings": 14,
    "TotalWickets": 18,
    "TotalOvers": 57.0,
    "MaidenOvers": 5,
    "Runs": 274,
    "Economy": 4.81,
    "Average": 15.22,
    "StrikeRate": 19.0,
    "DotballPercentage": 64.91,
    "WidesPercentage": 11.4,
    "NoBallPercentage": 1.75,
    "BoundaryPercentage": 6.43
  },
  {
    "Name": "Skd Rahul",
    "TotalInnings": 15,
    "TotalWickets": 14,
    "TotalOvers": 48.3,
    "MaidenOvers": 0,
    "Runs": 290,
    "Economy": 5.98,
    "Average": 20.71,
    "StrikeRate": 20.79,
    "DotballPercentage": 50.52,
    "WidesPercentage": 8.25,
    "NoBallPercentage": 1.03,
    "BoundaryPercentage": 7.9
  },
  {
    "Name": "Himel Barman",
    "TotalInnings": 13,
    "TotalWickets": 13,
    "TotalOvers": 43.0,
    "MaidenOvers": 2,
    "Runs": 236,
    "Economy": 5.49,
    "Average": 18.15,
    "StrikeRate": 19.85,
    "DotballPercentage": 51.55,
    "WidesPercentage": 12.02,
    "NoBallPercentage": 1.16,
    "BoundaryPercentage": 5.81
  },
  {
    "Name": "Khan Rahamatullah",
    "TotalInnings": 14,
    "TotalWickets": 11,
    "TotalOvers": 31.3,
    "MaidenOvers": 2,
    "Runs": 172,
    "Economy": 5.46,
    "Average": 15.64,
    "StrikeRate": 17.18,
    "DotballPercentage": 44.97,
    "WidesPercentage": 2.65,
    "NoBallPercentage": 0.0,
    "BoundaryPercentage": 5.82
  },
  {
    "Name": "Md Shariful Islam",
    "TotalInnings": 12,
    "TotalWickets": 9,
    "TotalOvers": 27.4,
    "MaidenOvers": 0,
    "Runs": 155,
    "Economy": 5.6,
    "Average": 17.22,
    "StrikeRate": 18.44,
    "DotballPercentage": 47.59,
    "WidesPercentage": 9.04,
    "NoBallPercentage": 0.6,
    "BoundaryPercentage": 5.42
  },
  {
    "Name": "Rakesh Mondal",
    "TotalInnings": 10,
    "TotalWickets": 6,
    "TotalOvers": 16.4,
    "MaidenOvers": 0,
    "Runs": 116,
    "Economy": 6.96,
    "Average": 19.33,
    "StrikeRate": 16.67,
    "DotballPercentage": 45.0,
    "WidesPercentage": 7.0,
    "NoBallPercentage": 3.0,
    "BoundaryPercentage": 8.0
  },
  {
    "Name": "Agnik Podder",
    "TotalInnings": 3,
    "TotalWickets": 5,
    "TotalOvers": 12.1,
    "MaidenOvers": 1,
    "Runs": 44,
    "Economy": 3.62,
    "Average": 8.8,
    "StrikeRate": 14.6,
    "DotballPercentage": 58.9,
    "WidesPercentage": 8.22,
    "NoBallPercentage": 0.0,
    "BoundaryPercentage": 1.37
  },
  {
    "Name": "Shaymal Paul Jackey",
    "TotalInnings": 7,
    "TotalWickets": 3,
    "TotalOvers": 14.1,
    "MaidenOvers": 0,
    "Runs": 97,
    "Economy": 6.85,
    "Average": 32.33,
    "StrikeRate": 28.33,
    "DotballPercentage": 49.41,
    "WidesPercentage": 22.35,
    "NoBallPercentage": 4.71,
    "BoundaryPercentage": 7.06
  },
  {
    "Name": "Muhitur Shuvo",
    "TotalInnings": 4,
    "TotalWickets": 3,
    "TotalOvers": 9.0,
    "MaidenOvers": 0,
    "Runs": 49,
    "Economy": 5.44,
    "Average": 16.33,
    "StrikeRate": 18.0,
    "DotballPercentage": 38.89,
    "WidesPercentage": 14.81,
    "NoBallPercentage": 0.0,
    "BoundaryPercentage": 1.85
  },
  {
    "Name": "Saikat Barua",
    "TotalInnings": 5,
    "TotalWickets": 2,
    "TotalOvers": 10.3,
    "MaidenOvers": 0,
    "Runs": 61,
    "Economy": 5.81,
    "Average": 30.5,
    "StrikeRate": 31.5,
    "DotballPercentage": 46.03,
    "WidesPercentage": 7.94,
    "NoBallPercentage": 1.59,
    "BoundaryPercentage": 4.76
  },
  {
    "Name": "Rokonuzzaman Tuhin",
    "TotalInnings": 3,
    "TotalWickets": 2,
    "TotalOvers": 5.0,
    "MaidenOvers": 0,
    "Runs": 29,
    "Economy": 5.8,
    "Average": 14.5,
    "StrikeRate": 15.0,
    "DotballPercentage": 60.0,
    "WidesPercentage": 10.0,
    "NoBallPercentage": 3.33,
    "BoundaryPercentage": 13.33
  },
  {
    "Name": "Shoybul Shakib",
    "TotalInnings": 4,
    "TotalWickets": 2,
    "TotalOvers": 8.0,
    "MaidenOvers": 0,
    "Runs": 57,
    "Economy": 7.12,
    "Average": 28.5,
    "StrikeRate": 24.0,
    "DotballPercentage": 37.5,
    "WidesPercentage": 8.33,
    "NoBallPercentage": 0.0,
    "BoundaryPercentage": 8.33
  },
  {
    "Name": "Abu Jahed Chowdhury",
    "TotalInnings": 4,
    "TotalWickets": 1,
    "TotalOvers": 5.0,
    "MaidenOvers": 0,
    "Runs": 34,
    "Economy": 6.8,
    "Average": 34.0,
    "StrikeRate": 30.0,
    "DotballPercentage": 33.33,
    "WidesPercentage": 3.33,
    "NoBallPercentage": 0.0,
    "BoundaryPercentage": 10.0
  },
  {
    "Name": "Jubayer Ibna Kalam",
    "TotalInnings": 1,
    "TotalWickets": 1,
    "TotalOvers": 2.0,
    "MaidenOvers": 0,
    "Runs": 11,
    "Economy": 5.5,
    "Average": 11.0,
    "StrikeRate": 12.0,
    "DotballPercentage": 58.33,
    "WidesPercentage": 8.33,
    "NoBallPercentage": 8.33,
    "BoundaryPercentage": 0.0
  },
  {
    "Name": "Khonodoker Ahmed Shaon",
    "TotalInnings": 4,
    "TotalWickets": 0,
    "TotalOvers": 12.0,
    "MaidenOvers": 0,
    "Runs": 64,
    "Economy": 5.33,
    "Average": "Infinity",
    "StrikeRate": "Infinity",
    "DotballPercentage": 43.06,
    "WidesPercentage": 0.0,
    "NoBallPercentage": 2.78,
    "BoundaryPercentage": 5.56
  }
]`

  dataP = `[
  {
    "Name": "Md Shariful Islam",
    "TotalInnings": 10,
    "TotalWickets": 10,
    "TotalOvers": 24.4,
    "MaidenOvers": 0,
    "Runs": 140,
    "Economy": 5.68,
    "Average": 14.0,
    "StrikeRate": 14.8,
    "DotballPercentage": 43.24,
    "WidesPercentage": 4.73,
    "NoBallPercentage": 0.0,
    "BoundaryPercentage": 7.43
  },
  {
    "Name": "Khan Rahamatullah",
    "TotalInnings": 12,
    "TotalWickets": 7,
    "TotalOvers": 31.4,
    "MaidenOvers": 0,
    "Runs": 218,
    "Economy": 6.88,
    "Average": 31.14,
    "StrikeRate": 27.14,
    "DotballPercentage": 41.58,
    "WidesPercentage": 11.05,
    "NoBallPercentage": 0.0,
    "BoundaryPercentage": 9.47
  },
  {
    "Name": "Farhad Mahbub",
    "TotalInnings": 11,
    "TotalWickets": 7,
    "TotalOvers": 40.0,
    "MaidenOvers": 0,
    "Runs": 268,
    "Economy": 6.7,
    "Average": 38.29,
    "StrikeRate": 34.29,
    "DotballPercentage": 50.42,
    "WidesPercentage": 10.0,
    "NoBallPercentage": 0.0,
    "BoundaryPercentage": 12.92
  },
  {
    "Name": "Rakesh Mondal",
    "TotalInnings": 10,
    "TotalWickets": 5,
    "TotalOvers": 28.5,
    "MaidenOvers": 0,
    "Runs": 182,
    "Economy": 6.31,
    "Average": 36.4,
    "StrikeRate": 34.6,
    "DotballPercentage": 44.51,
    "WidesPercentage": 11.56,
    "NoBallPercentage": 1.73,
    "BoundaryPercentage": 7.51
  },
  {
    "Name": "Skd Rahul",
    "TotalInnings": 9,
    "TotalWickets": 5,
    "TotalOvers": 23.2,
    "MaidenOvers": 1,
    "Runs": 177,
    "Economy": 7.59,
    "Average": 35.4,
    "StrikeRate": 28.0,
    "DotballPercentage": 40.0,
    "WidesPercentage": 5.0,
    "NoBallPercentage": 0.71,
    "BoundaryPercentage": 12.14
  },
  {
    "Name": "Suzon Parvez",
    "TotalInnings": 7,
    "TotalWickets": 5,
    "TotalOvers": 26.1,
    "MaidenOvers": 1,
    "Runs": 139,
    "Economy": 5.31,
    "Average": 27.8,
    "StrikeRate": 31.4,
    "DotballPercentage": 59.24,
    "WidesPercentage": 11.46,
    "NoBallPercentage": 0.64,
    "BoundaryPercentage": 9.55
  },
  {
    "Name": "Muhitur Shuvo",
    "TotalInnings": 8,
    "TotalWickets": 5,
    "TotalOvers": 16.0,
    "MaidenOvers": 0,
    "Runs": 114,
    "Economy": 7.12,
    "Average": 22.8,
    "StrikeRate": 19.2,
    "DotballPercentage": 43.75,
    "WidesPercentage": 13.54,
    "NoBallPercentage": 1.04,
    "BoundaryPercentage": 9.38
  },
  {
    "Name": "Himel Barman",
    "TotalInnings": 7,
    "TotalWickets": 4,
    "TotalOvers": 19.0,
    "MaidenOvers": 1,
    "Runs": 147,
    "Economy": 7.74,
    "Average": 36.75,
    "StrikeRate": 28.5,
    "DotballPercentage": 54.39,
    "WidesPercentage": 20.18,
    "NoBallPercentage": 2.63,
    "BoundaryPercentage": 16.67
  },
  {
    "Name": "Abu Jahed Chowdhury",
    "TotalInnings": 2,
    "TotalWickets": 2,
    "TotalOvers": 5.0,
    "MaidenOvers": 0,
    "Runs": 30,
    "Economy": 6.0,
    "Average": 15.0,
    "StrikeRate": 15.0,
    "DotballPercentage": 33.33,
    "WidesPercentage": 6.67,
    "NoBallPercentage": 0.0,
    "BoundaryPercentage": 6.67
  },
  {
    "Name": "Adrian Sharafi",
    "TotalInnings": 3,
    "TotalWickets": 2,
    "TotalOvers": 6.0,
    "MaidenOvers": 0,
    "Runs": 37,
    "Economy": 6.17,
    "Average": 18.5,
    "StrikeRate": 18.0,
    "DotballPercentage": 33.33,
    "WidesPercentage": 5.56,
    "NoBallPercentage": 0.0,
    "BoundaryPercentage": 5.56
  },
  {
    "Name": "Shoybul Shakib",
    "TotalInnings": 4,
    "TotalWickets": 2,
    "TotalOvers": 8.1,
    "MaidenOvers": 0,
    "Runs": 30,
    "Economy": 3.67,
    "Average": 15.0,
    "StrikeRate": 24.5,
    "DotballPercentage": 61.22,
    "WidesPercentage": 12.24,
    "NoBallPercentage": 0.0,
    "BoundaryPercentage": 2.04
  },
  {
    "Name": "Khonodoker Ahmed Shaon",
    "TotalInnings": 2,
    "TotalWickets": 1,
    "TotalOvers": 4.0,
    "MaidenOvers": 0,
    "Runs": 21,
    "Economy": 5.25,
    "Average": 21.0,
    "StrikeRate": 24.0,
    "DotballPercentage": 33.33,
    "WidesPercentage": 4.17,
    "NoBallPercentage": 0.0,
    "BoundaryPercentage": 0.0
  },
  {
    "Name": "Toslim Arif",
    "TotalInnings": 2,
    "TotalWickets": 0,
    "TotalOvers": 3.0,
    "MaidenOvers": 0,
    "Runs": 25,
    "Economy": 8.33,
    "Average": "Infinity",
    "StrikeRate": "Infinity",
    "DotballPercentage": 50.0,
    "WidesPercentage": 44.44,
    "NoBallPercentage": 0.0,
    "BoundaryPercentage": 11.11
  },
  {
    "Name": "Amlan Karmakar",
    "TotalInnings": 3,
    "TotalWickets": 0,
    "TotalOvers": 6.0,
    "MaidenOvers": 0,
    "Runs": 51,
    "Economy": 8.5,
    "Average": "Infinity",
    "StrikeRate": "Infinity",
    "DotballPercentage": 30.56,
    "WidesPercentage": 27.78,
    "NoBallPercentage": 5.56,
    "BoundaryPercentage": 5.56
  },
  {
    "Name": "Nazmul Mithun",
    "TotalInnings": 1,
    "TotalWickets": 0,
    "TotalOvers": 0.5,
    "MaidenOvers": 0,
    "Runs": 9,
    "Economy": 10.8,
    "Average": "Infinity",
    "StrikeRate": "Infinity",
    "DotballPercentage": 40.0,
    "WidesPercentage": 60.0,
    "NoBallPercentage": 40.0,
    "BoundaryPercentage": 0.0
  },
  {
    "Name": "Md Habibur Ra",
    "TotalInnings": 1,
    "TotalWickets": 0,
    "TotalOvers": 2.0,
    "MaidenOvers": 0,
    "Runs": 15,
    "Economy": 7.5,
    "Average": "Infinity",
    "StrikeRate": "Infinity",
    "DotballPercentage": 75.0,
    "WidesPercentage": 91.67,
    "NoBallPercentage": 0.0,
    "BoundaryPercentage": 0.0
  },
  {
    "Name": "Rokonuzzaman Tuhin",
    "TotalInnings": 2,
    "TotalWickets": 0,
    "TotalOvers": 2.0,
    "MaidenOvers": 0,
    "Runs": 32,
    "Economy": 16.0,
    "Average": "Infinity",
    "StrikeRate": "Infinity",
    "DotballPercentage": 8.33,
    "WidesPercentage": 41.67,
    "NoBallPercentage": 8.33,
    "BoundaryPercentage": 41.67
  },
  {
    "Name": "Jubayer Ibna Kalam",
    "TotalInnings": 1,
    "TotalWickets": 0,
    "TotalOvers": 1.0,
    "MaidenOvers": 0,
    "Runs": 12,
    "Economy": 12.0,
    "Average": "Infinity",
    "StrikeRate": "Infinity",
    "DotballPercentage": 66.67,
    "WidesPercentage": 66.67,
    "NoBallPercentage": 0.0,
    "BoundaryPercentage": 33.33
  },
  {
    "Name": "Salman Anwar",
    "TotalInnings": 1,
    "TotalWickets": 0,
    "TotalOvers": 1.0,
    "MaidenOvers": 0,
    "Runs": 16,
    "Economy": 16.0,
    "Average": "Infinity",
    "StrikeRate": "Infinity",
    "DotballPercentage": 0.0,
    "WidesPercentage": 33.33,
    "NoBallPercentage": 0.0,
    "BoundaryPercentage": 33.33
  },
  {
    "Name": "Saikat Barua",
    "TotalInnings": 1,
    "TotalWickets": 0,
    "TotalOvers": 1.0,
    "MaidenOvers": 0,
    "Runs": 7,
    "Economy": 7.0,
    "Average": "Infinity",
    "StrikeRate": "Infinity",
    "DotballPercentage": 33.33,
    "WidesPercentage": 0.0,
    "NoBallPercentage": 0.0,
    "BoundaryPercentage": 16.67
  },
  {
    "Name": "Shaymal Paul Jackey",
    "TotalInnings": 2,
    "TotalWickets": 0,
    "TotalOvers": 3.0,
    "MaidenOvers": 0,
    "Runs": 40,
    "Economy": 13.33,
    "Average": "Infinity",
    "StrikeRate": "Infinity",
    "DotballPercentage": 22.22,
    "WidesPercentage": 55.56,
    "NoBallPercentage": 5.56,
    "BoundaryPercentage": 22.22
  }
]
`

  columns: Array<GuiColumn> = [
    { header: 'Name', field: 'Name', width: "190" },
    { header: "Innings", field: "TotalInnings", width: "100" ,type: GuiDataType.NUMBER },
    { header: "Overs", field: "TotalOvers", width: "100", type: GuiDataType.NUMBER },
    { header: "Maiden", field: "MaidenOvers", width: "100", type: GuiDataType.NUMBER },
    { header: 'Wickets', field: 'TotalWickets', width: "100", type: GuiDataType.NUMBER },
    { header: 'Runs', field: 'Runs', width: "100", type: GuiDataType.NUMBER },
    {
      header: 'Average', field: 'Average', width: "120", type: GuiDataType.NUMBER,
      view: (value: number) => {
        if (value <= 20) {
          return `<div style="color: green;font-weight: bold">${value}</div>`;
        } else if (value >= 30) {
          return `<div style="color: red">${value}</div>`;
        } else {
          return `<div>${value}</div>`;
        }
      }
    },
    {
      header: "Strike Rate", field: "StrikeRate", width: "120", type: GuiDataType.NUMBER,
      view: (value: number) => {
        if (value <= 20) {
          return `<div style="color: green;font-weight: bold">${value}</div>`;
        } else if (value >= 30) {
          return `<div style="color: red">${value}</div>`;
        } else {
          return `<div>${value}</div>`;
        }
      }
    },
    {
      header: "Economy", field: "Economy", width: "120", type: GuiDataType.NUMBER,
      view: (value: number) => {
        if (value <= 5) {
          return `<div style="color: green;font-weight: bold">${value}</div>`;
        } else if (value >= 6.5) {
          return `<div style="color: red">${value}</div>`;
        } else {
          return `<div>${value}</div>`;
        }
      }
    },
    {
      header: 'Dots %', field: 'DotballPercentage', width: "120", type: GuiDataType.NUMBER,
      view: (value: number) => {
        if (value >= 50) {
          return `<div style="color: green;font-weight: bold">${value}</div>`;
        } else if (value <= 30) {
          return `<div style="color: red">${value}</div>`;
        } else {
          return `<div>${value}</div>`;
        }
      }
    },
    { header: 'Boundary %', field: 'BoundaryPercentage', width: "110", type: GuiDataType.NUMBER},
    {
      header: 'Wide %', field: 'WidesPercentage', width: "110", type: GuiDataType.NUMBER,
      view: (value: number) => {
        if (value <= 10) {
          return `<div style="color: green;font-weight: bold">${value}</div>`;
        } else if (value >= 20) {
          return `<div style="color: red">${value}</div>`;
        } else {
          return `<div>${value}</div>`;
        }
      }
    },
    {
      header: 'NoBall %', field: 'NoBallPercentage', width: "110", type: GuiDataType.NUMBER,
      view: (value: number) => {
        if (value < 5) {
          return `<div style="color: green;font-weight: bold">${value}</div>`;
        } else if (value >= 10) {
          return `<div style="color: red">${value}</div>`;
        } else {
          return `<div>${value}</div>`;
        }
      }
    }
   ];



  source: Array<any> = JSON.parse(this.data);
  sourceT: Array<any> = JSON.parse(this.dataT);
  sourceP: Array<any> = JSON.parse(this.dataP);

  sorting: GuiSorting = {
    enabled: true
  };
}
