import { Component } from '@angular/core';

import { GuiColumn, GuiDataType, GuiSorting } from '@generic-ui/ngx-grid';

//import * as data from "./OverallBattingPerformance.json";

declare var require: any

@Component({
  selector: 'app-batting-stats-position-component',
  templateUrl: './batting-stats-position.component.html',
  styleUrls: ['./batting-stats-position.component.css']
})

export class BattingStatsPositionComponent {

  data =`[
  {
    "Position": "Opening",
    "BattingFigures": [
      {
        "Name": "Farhad Mahbub",
        "TotalRuns": 278,
        "TotalBallFaced": 302,
        "TotalInnings": 12,
        "NotOuts": 1,
        "Average": 25.27,
        "StrikeRate": 92.05
      },
      {
        "Name": "Agnik Podder",
        "TotalRuns": 46,
        "TotalBallFaced": 67,
        "TotalInnings": 2,
        "NotOuts": 0,
        "Average": 23.0,
        "StrikeRate": 68.66
      },
      {
        "Name": "Md Shariful Islam",
        "TotalRuns": 175,
        "TotalBallFaced": 180,
        "TotalInnings": 10,
        "NotOuts": 2,
        "Average": 21.88,
        "StrikeRate": 97.22
      },
      {
        "Name": "Skd Rahul",
        "TotalRuns": 216,
        "TotalBallFaced": 213,
        "TotalInnings": 12,
        "NotOuts": 2,
        "Average": 21.6,
        "StrikeRate": 101.41
      },
      {
        "Name": "Rakesh Mondal",
        "TotalRuns": 148,
        "TotalBallFaced": 141,
        "TotalInnings": 11,
        "NotOuts": 1,
        "Average": 14.8,
        "StrikeRate": 104.96
      },
      {
        "Name": "Shoybul Shakib",
        "TotalRuns": 43,
        "TotalBallFaced": 36,
        "TotalInnings": 4,
        "NotOuts": 1,
        "Average": 14.33,
        "StrikeRate": 119.44
      },
      {
        "Name": "Khan Rahamatullah",
        "TotalRuns": 11,
        "TotalBallFaced": 8,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 11.0,
        "StrikeRate": 137.5
      },
      {
        "Name": "Aftab Risul",
        "TotalRuns": 2,
        "TotalBallFaced": 5,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 2.0,
        "StrikeRate": 40.0
      },
      {
        "Name": "Abu Jahed Chowdhury",
        "TotalRuns": 2,
        "TotalBallFaced": 4,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 2.0,
        "StrikeRate": 50.0
      },
      {
        "Name": "Suzon Parvez",
        "TotalRuns": 1,
        "TotalBallFaced": 3,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 1.0,
        "StrikeRate": 33.33
      }
    ]
  },
  {
    "Position": 3,
    "BattingFigures": [
      {
        "Name": "Shaymal Paul Jackey",
        "TotalRuns": 58,
        "TotalBallFaced": 64,
        "TotalInnings": 3,
        "NotOuts": 2,
        "Average": 58.0,
        "StrikeRate": 90.62
      },
      {
        "Name": "Rokonuzzaman Tuhin",
        "TotalRuns": 51,
        "TotalBallFaced": 69,
        "TotalInnings": 3,
        "NotOuts": 0,
        "Average": 17,
        "StrikeRate": 73.9
      },
      {
        "Name": "Suzon Parvez",
        "TotalRuns": 104,
        "TotalBallFaced": 81,
        "TotalInnings": 6,
        "NotOuts": 1,
        "Average": 20.8,
        "StrikeRate": 128.4
      },
      {
        "Name": "Farhad Mahbub",
        "TotalRuns": 185,
        "TotalBallFaced": 230,
        "TotalInnings": 10,
        "NotOuts": 0,
        "Average": 18.5,
        "StrikeRate": 80.43
      },
      {
        "Name": "Agnik Podder",
        "TotalRuns": 14,
        "TotalBallFaced": 18,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 14.0,
        "StrikeRate": 77.78
      },
      {
        "Name": "Shoybul Shakib",
        "TotalRuns": 13,
        "TotalBallFaced": 14,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 13.0,
        "StrikeRate": 92.86
      },
      {
        "Name": "Khonodoker Ahmed Shaon",
        "TotalRuns": 13,
        "TotalBallFaced": 28,
        "TotalInnings": 2,
        "NotOuts": 0,
        "Average": 6.5,
        "StrikeRate": 46.43
      },
      {
        "Name": "Saikat Barua",
        "TotalRuns": 6,
        "TotalBallFaced": 14,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 6.0,
        "StrikeRate": 42.86
      },
      {
        "Name": "Rakesh Mondal",
        "TotalRuns": 5,
        "TotalBallFaced": 5,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 5.0,
        "StrikeRate": 100.0
      },
      {
        "Name": "Md Shariful Islam",
        "TotalRuns": 11,
        "TotalBallFaced": 19,
        "TotalInnings": 3,
        "NotOuts": 0,
        "Average": 3.67,
        "StrikeRate": 57.89
      },
      {
        "Name": "Abu Jahed Chowdhury",
        "TotalRuns": 2,
        "TotalBallFaced": 11,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 2.0,
        "StrikeRate": 18.18
      }
    ]
  },
  {
    "Position": 4,
    "BattingFigures": [
      {
        "Name": "Chinmoy Saha",
        "TotalRuns": 31,
        "TotalBallFaced": 46,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 31.0,
        "StrikeRate": 67.39
      },
      {
        "Name": "Suzon Parvez",
        "TotalRuns": 26,
        "TotalBallFaced": 25,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 26.0,
        "StrikeRate": 104.0
      },
      {
        "Name": "Skd Rahul",
        "TotalRuns": 211,
        "TotalBallFaced": 233,
        "TotalInnings": 14,
        "NotOuts": 3,
        "Average": 19.18,
        "StrikeRate": 90.56
      },
      {
        "Name": "Rakesh Mondal",
        "TotalRuns": 22,
        "TotalBallFaced": 30,
        "TotalInnings": 2,
        "NotOuts": 0,
        "Average": 11.0,
        "StrikeRate": 73.33
      },
      {
        "Name": "Himel Barman",
        "TotalRuns": 11,
        "TotalBallFaced": 18,
        "TotalInnings": 1,
        "NotOuts": 1,
        "Average": 11.0,
        "StrikeRate": 61.11
      },
      {
        "Name": "Rokonuzzaman Tuhin",
        "TotalRuns": 57,
        "TotalBallFaced": 101,
        "TotalInnings": 6,
        "NotOuts": 1,
        "Average": 11.4,
        "StrikeRate": 56.43
      },
      {
        "Name": "Khonodoker Ahmed Shaon",
        "TotalRuns": 9,
        "TotalBallFaced": 22,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 9.0,
        "StrikeRate": 40.91
      },
      {
        "Name": "Adrian Sharafi",
        "TotalRuns": 9,
        "TotalBallFaced": 15,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 9.0,
        "StrikeRate": 60.0
      },
      {
        "Name": "Agnik Podder",
        "TotalRuns": 7,
        "TotalBallFaced": 21,
        "TotalInnings": 2,
        "NotOuts": 0,
        "Average": 3.5,
        "StrikeRate": 33.33
      },
      {
        "Name": "Khan Rahamatullah",
        "TotalRuns": 3,
        "TotalBallFaced": 7,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 3.0,
        "StrikeRate": 42.86
      },
      {
        "Name": "Abu Jahed Chowdhury",
        "TotalRuns": 5,
        "TotalBallFaced": 7,
        "TotalInnings": 2,
        "NotOuts": 0,
        "Average": 2.5,
        "StrikeRate": 71.43
      },
      {
        "Name": "Farhad Mahbub",
        "TotalRuns": 1,
        "TotalBallFaced": 7,
        "TotalInnings": 2,
        "NotOuts": 0,
        "Average": 0.5,
        "StrikeRate": 14.29
      }
    ]
  },
  {
    "Position": 5,
    "BattingFigures": [
      {
        "Name": "Rokonuzzaman Tuhin",
        "TotalRuns": 135,
        "TotalBallFaced": 141,
        "TotalInnings": 4,
        "NotOuts": 1,
        "Average": 45,
        "StrikeRate": 95.74
      },
      {
        "Name": "Himel Barman",
        "TotalRuns": 40,
        "TotalBallFaced": 59,
        "TotalInnings": 3,
        "NotOuts": 2,
        "Average": 40.0,
        "StrikeRate": 67.8
      },
      {
        "Name": "Chinmoy Saha",
        "TotalRuns": 33,
        "TotalBallFaced": 34,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 33.0,
        "StrikeRate": 97.06
      },
      {
        "Name": "Shaymal Paul Jackey",
        "TotalRuns": 133,
        "TotalBallFaced": 115,
        "TotalInnings": 5,
        "NotOuts": 0,
        "Average": 26.6,
        "StrikeRate": 115.65
      },
      {
        "Name": "Rakesh Mondal",
        "TotalRuns": 19,
        "TotalBallFaced": 18,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 19.0,
        "StrikeRate": 105.56
      },
      {
        "Name": "Md Shariful Islam",
        "TotalRuns": 18,
        "TotalBallFaced": 9,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 18.0,
        "StrikeRate": 200.0
      },
      {
        "Name": "Abu Jahed Chowdhury",
        "TotalRuns": 16,
        "TotalBallFaced": 18,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 16.0,
        "StrikeRate": 88.89
      },
      {
        "Name": "Suzon Parvez",
        "TotalRuns": 45,
        "TotalBallFaced": 73,
        "TotalInnings": 6,
        "NotOuts": 1,
        "Average": 9.0,
        "StrikeRate": 61.64
      },
      {
        "Name": "Muhitur Shuvo",
        "TotalRuns": 6,
        "TotalBallFaced": 11,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 6.0,
        "StrikeRate": 54.55
      },
      {
        "Name": "Khonodoker Ahmed Shaon",
        "TotalRuns": 3,
        "TotalBallFaced": 9,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 3.0,
        "StrikeRate": 33.33
      },
      {
        "Name": "Farhad Mahbub",
        "TotalRuns": 1,
        "TotalBallFaced": 2,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 1.0,
        "StrikeRate": 50.0
      },
      {
        "Name": "Khan Rahamatullah",
        "TotalRuns": 0,
        "TotalBallFaced": 1,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 0.0,
        "StrikeRate": 0.0
      },
      {
        "Name": "Shoybul Shakib",
        "TotalRuns": 0,
        "TotalBallFaced": 1,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 0.0,
        "StrikeRate": 0.0
      }
    ]
  },
  {
    "Position": 6,
    "BattingFigures": [
      {
        "Name": "Rakesh Mondal",
        "TotalRuns": 98,
        "TotalBallFaced": 92,
        "TotalInnings": 4,
        "NotOuts": 1,
        "Average": 32.67,
        "StrikeRate": 106.52
      },
      {
        "Name": "Farhad Mahbub",
        "TotalRuns": 30,
        "TotalBallFaced": 33,
        "TotalInnings": 1,
        "NotOuts": 1,
        "Average": 30.0,
        "StrikeRate": 90.91
      },
      {
        "Name": "Shaymal Paul Jackey",
        "TotalRuns": 53,
        "TotalBallFaced": 72,
        "TotalInnings": 4,
        "NotOuts": 2,
        "Average": 26.5,
        "StrikeRate": 73.61
      },
      {
        "Name": "Himel Barman",
        "TotalRuns": 69,
        "TotalBallFaced": 49,
        "TotalInnings": 3,
        "NotOuts": 0,
        "Average": 23.0,
        "StrikeRate": 140.82
      },
      {
        "Name": "Suzon Parvez",
        "TotalRuns": 83,
        "TotalBallFaced": 68,
        "TotalInnings": 4,
        "NotOuts": 0,
        "Average": 20.75,
        "StrikeRate": 122.06
      },
      {
        "Name": "Abu Jahed Chowdhury",
        "TotalRuns": 47,
        "TotalBallFaced": 63,
        "TotalInnings": 5,
        "NotOuts": 1,
        "Average": 11.75,
        "StrikeRate": 74.6
      },
      {
        "Name": "Shoybul Shakib",
        "TotalRuns": 23,
        "TotalBallFaced": 24,
        "TotalInnings": 2,
        "NotOuts": 0,
        "Average": 11.5,
        "StrikeRate": 95.83
      },
      {
        "Name": "Rokonuzzaman Tuhin",
        "TotalRuns": 34,
        "TotalBallFaced": 52,
        "TotalInnings": 3,
        "NotOuts": 0,
        "Average": 11.33,
        "StrikeRate": 65.38
      },
      {
        "Name": "Md Habibur Ra",
        "TotalRuns": 10,
        "TotalBallFaced": 12,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 10.0,
        "StrikeRate": 83.33
      },
      {
        "Name": "Khan Rahamatullah",
        "TotalRuns": 2,
        "TotalBallFaced": 4,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 2.0,
        "StrikeRate": 50.0
      }
    ]
  },
  {
    "Position": 7,
    "BattingFigures": [
      {
        "Name": "Suzon Parvez",
        "TotalRuns": 73,
        "TotalBallFaced": 49,
        "TotalInnings": 3,
        "NotOuts": 2,
        "Average": 73.0,
        "StrikeRate": 148.98
      },
      {
        "Name": "Rakesh Mondal",
        "TotalRuns": 46,
        "TotalBallFaced": 30,
        "TotalInnings": 1,
        "NotOuts": 1,
        "Average": 46.0,
        "StrikeRate": 153.33
      },
      {
        "Name": "Abu Jahed Chowdhury",
        "TotalRuns": 31,
        "TotalBallFaced": 37,
        "TotalInnings": 2,
        "NotOuts": 1,
        "Average": 31.0,
        "StrikeRate": 83.78
      },
      {
        "Name": "Rokonuzzaman Tuhin",
        "TotalRuns": 25,
        "TotalBallFaced": 41,
        "TotalInnings": 3,
        "NotOuts": 0,
        "Average": 8.33,
        "StrikeRate": 60.98
      },
      {
        "Name": "Himel Barman",
        "TotalRuns": 48,
        "TotalBallFaced": 56,
        "TotalInnings": 8,
        "NotOuts": 1,
        "Average": 6.86,
        "StrikeRate": 85.71
      },
      {
        "Name": "Chinmoy Saha",
        "TotalRuns": 6,
        "TotalBallFaced": 13,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 6.0,
        "StrikeRate": 46.15
      },
      {
        "Name": "Khonodoker Ahmed Shaon",
        "TotalRuns": 15,
        "TotalBallFaced": 41,
        "TotalInnings": 3,
        "NotOuts": 0,
        "Average": 5.0,
        "StrikeRate": 36.59
      },
      {
        "Name": "Md Shariful Islam",
        "TotalRuns": 9,
        "TotalBallFaced": 10,
        "TotalInnings": 2,
        "NotOuts": 0,
        "Average": 4.5,
        "StrikeRate": 90.0
      },
      {
        "Name": "Saikat Barua",
        "TotalRuns": 3,
        "TotalBallFaced": 6,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 3.0,
        "StrikeRate": 50.0
      }
    ]
  },
  {
    "Position": 8,
    "BattingFigures": [
      {
        "Name": "Abu Jahed Chowdhury",
        "TotalRuns": 32,
        "TotalBallFaced": 56,
        "TotalInnings": 3,
        "NotOuts": 3,
        "Average": 32.0,
        "StrikeRate": 57.14
      },
      {
        "Name": "Rokonuzzaman Tuhin",
        "TotalRuns": 23,
        "TotalBallFaced": 32,
        "TotalInnings": 3,
        "NotOuts": 2,
        "Average": 23.0,
        "StrikeRate": 71.88
      },
      {
        "Name": "Khonodoker Ahmed Shaon",
        "TotalRuns": 18,
        "TotalBallFaced": 19,
        "TotalInnings": 2,
        "NotOuts": 1,
        "Average": 18.0,
        "StrikeRate": 94.74
      },
      {
        "Name": "Rakesh Mondal",
        "TotalRuns": 52,
        "TotalBallFaced": 46,
        "TotalInnings": 3,
        "NotOuts": 0,
        "Average": 17.33,
        "StrikeRate": 113.04
      },
      {
        "Name": "Chinmoy Saha",
        "TotalRuns": 14,
        "TotalBallFaced": 13,
        "TotalInnings": 2,
        "NotOuts": 1,
        "Average": 14.0,
        "StrikeRate": 107.69
      },
      {
        "Name": "Himel Barman",
        "TotalRuns": 20,
        "TotalBallFaced": 21,
        "TotalInnings": 3,
        "NotOuts": 1,
        "Average": 10.0,
        "StrikeRate": 95.24
      },
      {
        "Name": "Amlan Karmakar",
        "TotalRuns": 6,
        "TotalBallFaced": 9,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 6.0,
        "StrikeRate": 66.67
      },
      {
        "Name": "Shaymal Paul Jackey",
        "TotalRuns": 4,
        "TotalBallFaced": 5,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 4.0,
        "StrikeRate": 80.0
      },
      {
        "Name": "Toslim Arif 03:32",
        "TotalRuns": 3,
        "TotalBallFaced": 3,
        "TotalInnings": 1,
        "NotOuts": 1,
        "Average": 3.0,
        "StrikeRate": 100.0
      },
      {
        "Name": "Nazmul Mithun",
        "TotalRuns": 3,
        "TotalBallFaced": 4,
        "TotalInnings": 1,
        "NotOuts": 1,
        "Average": 3.0,
        "StrikeRate": 75.0
      },
      {
        "Name": "Md Shariful Islam",
        "TotalRuns": 5,
        "TotalBallFaced": 6,
        "TotalInnings": 4,
        "NotOuts": 2,
        "Average": 2.5,
        "StrikeRate": 83.33
      },
      {
        "Name": "Muhitur Shuvo",
        "TotalRuns": 2,
        "TotalBallFaced": 3,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 2.0,
        "StrikeRate": 66.67
      },
      {
        "Name": "Khan Rahamatullah",
        "TotalRuns": 2,
        "TotalBallFaced": 5,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 2.0,
        "StrikeRate": 40.0
      }
    ]
  },
  {
    "Position": 9,
    "BattingFigures": [
      {
        "Name": "Khonodoker Ahmed Shaon",
        "TotalRuns": 45,
        "TotalBallFaced": 37,
        "TotalInnings": 2,
        "NotOuts": 1,
        "Average": 45.0,
        "StrikeRate": 121.62
      },
      {
        "Name": "Himel Barman",
        "TotalRuns": 72,
        "TotalBallFaced": 65,
        "TotalInnings": 4,
        "NotOuts": 2,
        "Average": 36.0,
        "StrikeRate": 110.77
      },
      {
        "Name": "Rokonuzzaman Tuhin",
        "TotalRuns": 15,
        "TotalBallFaced": 5,
        "TotalInnings": 1,
        "NotOuts": 1,
        "Average": 15.0,
        "StrikeRate": 300.0
      },
      {
        "Name": "Farhad Mahbub",
        "TotalRuns": 13,
        "TotalBallFaced": 11,
        "TotalInnings": 1,
        "NotOuts": 1,
        "Average": 13.0,
        "StrikeRate": 118.18
      },
      {
        "Name": "Shoybul Shakib",
        "TotalRuns": 9,
        "TotalBallFaced": 12,
        "TotalInnings": 2,
        "NotOuts": 2,
        "Average": 9.0,
        "StrikeRate": 75.0
      },
      {
        "Name": "Abu Jahed Chowdhury",
        "TotalRuns": 6,
        "TotalBallFaced": 8,
        "TotalInnings": 1,
        "NotOuts": 1,
        "Average": 6.0,
        "StrikeRate": 75.0
      },
      {
        "Name": "Saikat Barua",
        "TotalRuns": 5,
        "TotalBallFaced": 16,
        "TotalInnings": 2,
        "NotOuts": 1,
        "Average": 5.0,
        "StrikeRate": 31.25
      },
      {
        "Name": "Chinmoy Saha",
        "TotalRuns": 4,
        "TotalBallFaced": 2,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 4.0,
        "StrikeRate": 200.0
      },
      {
        "Name": "Md Shariful Islam",
        "TotalRuns": 7,
        "TotalBallFaced": 13,
        "TotalInnings": 2,
        "NotOuts": 0,
        "Average": 3.5,
        "StrikeRate": 53.85
      },
      {
        "Name": "Shaymal Paul Jackey",
        "TotalRuns": 1,
        "TotalBallFaced": 1,
        "TotalInnings": 1,
        "NotOuts": 1,
        "Average": 1.0,
        "StrikeRate": 100.0
      },
      {
        "Name": "Jubayer Ibna Kalam",
        "TotalRuns": 0,
        "TotalBallFaced": 1,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 0.0,
        "StrikeRate": 0.0
      }
    ]
  },
  {
    "Position": 10,
    "BattingFigures": [
      {
        "Name": "Abu Jahed Chowdhury",
        "TotalRuns": 23,
        "TotalBallFaced": 24,
        "TotalInnings": 1,
        "NotOuts": 1,
        "Average": 23.0,
        "StrikeRate": 95.83
      },
      {
        "Name": "Khan Rahamatullah",
        "TotalRuns": 33,
        "TotalBallFaced": 30,
        "TotalInnings": 4,
        "NotOuts": 2,
        "Average": 16.5,
        "StrikeRate": 110.0
      },
      {
        "Name": "Md Shariful Islam",
        "TotalRuns": 30,
        "TotalBallFaced": 23,
        "TotalInnings": 3,
        "NotOuts": 1,
        "Average": 15.0,
        "StrikeRate": 130.43
      },
      {
        "Name": "Saikat Barua",
        "TotalRuns": 7,
        "TotalBallFaced": 7,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 7.0,
        "StrikeRate": 100.0
      },
      {
        "Name": "Shoybul Shakib",
        "TotalRuns": 5,
        "TotalBallFaced": 7,
        "TotalInnings": 1,
        "NotOuts": 1,
        "Average": 5.0,
        "StrikeRate": 71.43
      },
      {
        "Name": "Jubayer Ibna Kalam",
        "TotalRuns": 0,
        "TotalBallFaced": 1,
        "TotalInnings": 1,
        "NotOuts": 0,
        "Average": 0.0,
        "StrikeRate": 0.0
      },
      {
        "Name": "Muhitur Shuvo",
        "TotalRuns": 0,
        "TotalBallFaced": 2,
        "TotalInnings": 1,
        "NotOuts": 1,
        "Average": 0.0,
        "StrikeRate": 0.0
      }
    ]
  },
  {
    "Position": 11,
    "BattingFigures": [
      {
        "Name": "Salman Anwar",
        "TotalRuns": 11,
        "TotalBallFaced": 6,
        "TotalInnings": 1,
        "NotOuts": 1,
        "Average": 11.0,
        "StrikeRate": 183.33
      },
      {
        "Name": "Khan Rahamatullah",
        "TotalRuns": 7,
        "TotalBallFaced": 9,
        "TotalInnings": 3,
        "NotOuts": 2,
        "Average": 7.0,
        "StrikeRate": 77.78
      },
      {
        "Name": "Nazmul Mithun",
        "TotalRuns": 5,
        "TotalBallFaced": 4,
        "TotalInnings": 2,
        "NotOuts": 2,
        "Average": 5.0,
        "StrikeRate": 125.0
      },
      {
        "Name": "Muhitur Shuvo",
        "TotalRuns": 5,
        "TotalBallFaced": 11,
        "TotalInnings": 2,
        "NotOuts": 0,
        "Average": 2.5,
        "StrikeRate": 45.45
      }
    ]
  }
]
`
  columns: Array<GuiColumn> = [
    { header: 'Name', field: 'Name', width: "200", },
    { header: "Innings", field: "TotalInnings", width: "120", type: GuiDataType.NUMBER },
    { header: 'Not Out', field: 'NotOuts', width: "120", type: GuiDataType.NUMBER },
    { header: 'Total Runs', field: 'TotalRuns', width: "120", type: GuiDataType.NUMBER },
    {
      header: 'Average', field: 'Average', width: "120", type: GuiDataType.NUMBER,
      view: (value: number) => {
        if (value >= 25) {
          return `<div style="color: green;font-weight: bold">${value}</div>`;
        } else if (value < 10) {
          return `<div style="color: red">${value}</div>`;
        } else {
          return `<div>${value}</div>`;
        }
      }
    },
    {
      header: "Strike Rate", field: "StrikeRate", width: "120", type: GuiDataType.NUMBER,
      view: (value: number) => {
        if (value >= 90) {
          return `<div style="color: green;font-weight: bold">${value}</div>`;
        } else if (value < 70 && value >= 60) {
          return `<div style="color: orange">${value}</div>`;
        }
        else if (value < 60) {
          return `<div style="color: red">${value}</div>`;
        } else {
          return `<div>${value}</div>`;
        }
      }
    }
  ];
  sourceOpening: Array<any> = JSON.parse(this.data)[0].BattingFigures;
  source3: Array<any> = JSON.parse(this.data)[1].BattingFigures;
  source4: Array<any> = JSON.parse(this.data)[2].BattingFigures;
  source5: Array<any> = JSON.parse(this.data)[3].BattingFigures;
  source6: Array<any> = JSON.parse(this.data)[4].BattingFigures;
  source7: Array<any> = JSON.parse(this.data)[5].BattingFigures;
  source8: Array<any> = JSON.parse(this.data)[6].BattingFigures;
  source9: Array<any> = JSON.parse(this.data)[7].BattingFigures;
  source10: Array<any> = JSON.parse(this.data)[8].BattingFigures;
  source11: Array<any> = JSON.parse(this.data)[9].BattingFigures;

  sorting: GuiSorting = {
    enabled: true
  };
}
