import { Component, OnInit } from '@angular/core';

import { GuiColumn, GuiDataType, GuiSorting } from '@generic-ui/ngx-grid';

import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-batting-stats-partnerships--component',
  templateUrl: './batting-stats-partnerships.component.html',
  styleUrls: ['./batting-stats-partnerships.component.css']
})

export class BattingStatsPartnershipsComponent implements OnInit {

  constructor(private httpClient: HttpClient) { }

  columnsPartner: Array<GuiColumn> = [
    { header: 'Batter1', field: 'Batter1', width: "190" },
    { header: 'Batter2', field: 'Batter2', width: "190" },
    { header: "Runs", field: "Runs", width: "90", type: GuiDataType.NUMBER },
    { header: 'Ball Count', field: 'BallCount', width: "150", type: GuiDataType.NUMBER },
    { header: 'Partnership@Wicket', field: 'PartnershipWicket', width: "150", type: GuiDataType.NUMBER },
    { header: 'Opposition', field: 'Opposition', width: "150", type: GuiDataType.NUMBER }
  ]

  columnsAppearance: Array<GuiColumn> = [
    { header: 'Name', field: 'Name', width: "190" },
    { header: 'Count', field: 'Count', width: "190" }
  ]

  sourcePartner: Array<any> = [];
  sourceAppearance: Array<any> = [];

  sorting: GuiSorting = {
    enabled: true
  };

  ngOnInit() {
    this.httpClient.get("assets/SYDNEY FIGHTER-PartnershipStats.json").subscribe(data => {
      var v: any = JSON.parse(JSON.stringify(data));
      this.sourcePartner = v.Top20Partnerships;
      this.sourceAppearance = v.AppearanceCount;
    })
  }
}
