import { Component, OnInit} from '@angular/core';

import { GuiColumn, GuiDataType, GuiSorting } from '@generic-ui/ngx-grid';

import { HttpClient } from "@angular/common/http";

import * as ass from "../../assets/OverallBattingPerformance.json";


@Component({
  selector: 'app-batting-stats-component',
  templateUrl: './batting-stats.component.html',
  styleUrls: ['./batting-stats.component.css']
})

export class BattingStatsComponent implements OnInit {

  constructor(private httpClient: HttpClient) { }

  columns: Array<GuiColumn> = [
    { header: 'Name', field: 'Name', width:"190" },
    { header: "Innings", field: "TotalInnings", width: "90", type: GuiDataType.NUMBER },
    { header: 'Not Out', field: 'NotOuts', width: "90", type: GuiDataType.NUMBER },
    { header: 'Total Runs', field: 'TotalRuns', width: "100", type: GuiDataType.NUMBER },
    {
      header: 'Average', field: 'Average', width: "110", type: GuiDataType.NUMBER,
      view: (value: number) => {
        if (value >= 20) {
          return `<div style="color: green;font-weight: bold">${value}</div>`;
        } else if (value < 10) {
          return `<div style="color: red">${value}</div>`;
        } else {
          return `<div>${value}</div>`;
        }
      }
    },
    {
      header: "Strike Rate", field: "StrikeRate", width: "110", type: GuiDataType.NUMBER,
      view: (value: number) => {
        if (value >= 80) {
          return `<div style="color: green;font-weight: bold">${value}</div>`;
        } else if (value < 70 && value >= 60) {
          return `<div style="color: orange">${value}</div>`;
        }
        else if (value < 60) {
          return `<div style="color: red">${value}</div>`;
        } else {
          return `<div>${value}</div>`;
        }
      }
    },
    {
      header: 'Dots %', field: 'DotballPercentage', width: "90", type: GuiDataType.NUMBER,
      view: (value: number) => {
        if (value >= 50) {
          return `<div style="color: red;font-weight: bold">${value}</div>`;
        } else if (value < 40) {
          return `<div style="color: green;font-weight: bold">${value}</div>`;
        } else {
          return `<div>${value}</div>`;
        }
      }
    },
    { header: '1s %', field: 'SinglesPercentage', width: "80", type: GuiDataType.NUMBER },
    { header: '2s %', field: 'DoublesPercentage', width: "80", type: GuiDataType.NUMBER },
    { header: '3s %', field: 'TripplesPercentage', width: "80", type: GuiDataType.NUMBER },
    { header: '4s %', field: 'BoundaryPercentage', width: "80", type: GuiDataType.NUMBER },
    { header: '6s %', field: 'OverBoundaryPercentage', width: "80", type: GuiDataType.NUMBER },
    {
      header: '10+ runs Innings %', field: 'DoubleFigureInningsPercentage', width: "170", type: GuiDataType.NUMBER,
      view: (value: number) => {
        if (value >= 60) {
          return `<div style="color: green;font-weight: bold">${value}</div>`;
        } else if (value < 40) {
          return `<div style="color: red">${value}</div>`;
        } else {
          return `<div>${value}</div>`;
        }
      }
    },
    { header: '15+ balls survival %', field: 'FifteenBallInningsPercentage', width: "170", type: GuiDataType.NUMBER },

  ];
  
  //source: Array<any> = Array.from(ass);
  source: Array<any> = [];
  sourceT: Array<any> = [];
  sourceP: Array<any> = [];


  sorting: GuiSorting = {
    enabled: true
  };

   ngOnInit() {
    this.httpClient.get("assets/OverallBattingPerformance.json").subscribe(data => {
      this.source = JSON.parse(JSON.stringify(data));
    })
     console.log(ass);
     this.httpClient.get("assets/TournamentBattingPerformance.json").subscribe(data => {
       this.sourceT = JSON.parse(JSON.stringify(data));
     })

     this.httpClient.get("assets/FriendlyBattingPerformance.json").subscribe(data => {
       this.sourceP = JSON.parse(JSON.stringify(data));
     })
  }
}
