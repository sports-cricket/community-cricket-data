import { Component, OnInit } from '@angular/core';

import { GuiColumn, GuiDataType, GuiSorting } from '@generic-ui/ngx-grid';

import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-players-stats-allteams-component',
  templateUrl: './players-stats-allteams.component.html',
  styleUrls: ['./players-stats-allteams.component.css']
})

export class PlayerStatsAllTeamsComponent implements OnInit {

  constructor(private httpClient: HttpClient) { }


  teams: Array<any> = [
    { id: "SYDNEY FIGHTERS", name: "Sydney Fighters" },
    { id: "POWER SOURCE", name: "Power Source" },
    { id: "EKATTOR 71", name: "Ekattor 71" },
    { id: "PIFI", name: "Pifi" },
    { id: "SBT", name: "SBT" },
    { id: "COMILLA VICTORIANS", name: "Comilla Victorians" }
  ];
  statTypes: Array<any> = [
    { id: "Batting", name: "Player Stat- Batting" },
    { id: "Bowling", name: "Player Stat- Bowling"}
  ];

  sourceBatting: Array<any> = [];
  sourceBowling: Array<any> = [];
  selectedTeam: string = "SYDNEY FIGHTERS";
  selectedStat: string = "Batting";

  results: Array<any> = [];
  bowlingData: Array<any> = [];

  columns: Array<GuiColumn> = [];

  columnsBatting: Array<GuiColumn> = [
    { header: 'Name', field: 'Name', width: "190" },
    { header: "Innings", field: "TotalInnings", width: "90", type: GuiDataType.NUMBER },
    { header: 'Not Out', field: 'NotOuts', width: "90", type: GuiDataType.NUMBER },
    { header: 'Total Runs', field: 'TotalRuns', width: "100", type: GuiDataType.NUMBER },
    {
      header: 'Average', field: 'Average', width: "110", type: GuiDataType.NUMBER,
      view: (value: number) => {
        if (value >= 20) {
          return `<div style="color: green;font-weight: bold">${value}</div>`;
        } else if (value < 10) {
          return `<div style="color: red">${value}</div>`;
        } else {
          return `<div>${value}</div>`;
        }
      }
    },
    {
      header: "Strike Rate", field: "StrikeRate", width: "110", type: GuiDataType.NUMBER,
      view: (value: number) => {
        if (value >= 80) {
          return `<div style="color: green;font-weight: bold">${value}</div>`;
        } else if (value < 70 && value >= 60) {
          return `<div style="color: orange">${value}</div>`;
        }
        else if (value < 60) {
          return `<div style="color: red">${value}</div>`;
        } else {
          return `<div>${value}</div>`;
        }
      }
    },
    {
      header: 'Dots %', field: 'DotballPercentage', width: "90", type: GuiDataType.NUMBER,
      view: (value: number) => {
        if (value >= 50) {
          return `<div style="color: red;font-weight: bold">${value}</div>`;
        } else if (value < 40) {
          return `<div style="color: green;font-weight: bold">${value}</div>`;
        } else {
          return `<div>${value}</div>`;
        }
      }
    },
    { header: '1s %', field: 'SinglesPercentage', width: "80", type: GuiDataType.NUMBER },
    { header: '2s %', field: 'DoublesPercentage', width: "80", type: GuiDataType.NUMBER },
    { header: '3s %', field: 'TripplesPercentage', width: "80", type: GuiDataType.NUMBER },
    { header: '4s %', field: 'BoundaryPercentage', width: "80", type: GuiDataType.NUMBER },
    { header: '6s %', field: 'OverBoundaryPercentage', width: "80", type: GuiDataType.NUMBER },
    {
      header: '10+ runs Innings %', field: 'DoubleFigureInningsPercentage', width: "170", type: GuiDataType.NUMBER,
      view: (value: number) => {
        if (value >= 60) {
          return `<div style="color: green;font-weight: bold">${value}</div>`;
        } else if (value < 40) {
          return `<div style="color: red">${value}</div>`;
        } else {
          return `<div>${value}</div>`;
        }
      }
    },
    { header: '15+ balls survival %', field: 'FifteenBallInningsPercentage', width: "170", type: GuiDataType.NUMBER },

  ];


  columnsBowling: Array<GuiColumn> = [
    { header: 'Name', field: 'Name', width: "190" },
    { header: "Innings", field: "TotalInnings", width: "100", type: GuiDataType.NUMBER },
    { header: "Overs", field: "TotalOvers", width: "100", type: GuiDataType.NUMBER },
    { header: "Maiden", field: "MaidenOvers", width: "100", type: GuiDataType.NUMBER },
    { header: 'Wickets', field: 'TotalWickets', width: "100", type: GuiDataType.NUMBER },
    { header: 'Runs', field: 'Runs', width: "100", type: GuiDataType.NUMBER },
    {
      header: 'Average', field: 'Average', width: "120", type: GuiDataType.NUMBER,
      view: (value: number) => {
        if (value <= 20) {
          return `<div style="color: green;font-weight: bold">${value}</div>`;
        } else if (value >= 30) {
          return `<div style="color: red">${value}</div>`;
        } else {
          return `<div>${value}</div>`;
        }
      }
    },
    {
      header: "Strike Rate", field: "StrikeRate", width: "120", type: GuiDataType.NUMBER,
      view: (value: number) => {
        if (value <= 20) {
          return `<div style="color: green;font-weight: bold">${value}</div>`;
        } else if (value >= 30) {
          return `<div style="color: red">${value}</div>`;
        } else {
          return `<div>${value}</div>`;
        }
      }
    },
    {
      header: "Economy", field: "Economy", width: "120", type: GuiDataType.NUMBER,
      view: (value: number) => {
        if (value <= 5) {
          return `<div style="color: green;font-weight: bold">${value}</div>`;
        } else if (value >= 6.5) {
          return `<div style="color: red">${value}</div>`;
        } else {
          return `<div>${value}</div>`;
        }
      }
    },
    {
      header: 'Dots %', field: 'DotballPercentage', width: "120", type: GuiDataType.NUMBER,
      view: (value: number) => {
        if (value >= 50) {
          return `<div style="color: green;font-weight: bold">${value}</div>`;
        } else if (value <= 30) {
          return `<div style="color: red">${value}</div>`;
        } else {
          return `<div>${value}</div>`;
        }
      }
    },
    { header: 'Boundary %', field: 'BoundaryPercentage', width: "110", type: GuiDataType.NUMBER },
    {
      header: 'Wide %', field: 'WidesPercentage', width: "110", type: GuiDataType.NUMBER,
      view: (value: number) => {
        if (value <= 10) {
          return `<div style="color: green;font-weight: bold">${value}</div>`;
        } else if (value >= 20) {
          return `<div style="color: red">${value}</div>`;
        } else {
          return `<div>${value}</div>`;
        }
      }
    },
    {
      header: 'NoBall %', field: 'NoBallPercentage', width: "110", type: GuiDataType.NUMBER,
      view: (value: number) => {
        if (value < 5) {
          return `<div style="color: green;font-weight: bold">${value}</div>`;
        } else if (value >= 10) {
          return `<div style="color: red">${value}</div>`;
        } else {
          return `<div>${value}</div>`;
        }
      }
    }
  ];

  sorting: GuiSorting = {
    enabled: true
  };

  ngOnInit() {
    this.loadStats();
     
  }
  onChangeTeam(team:any) {
    
    this.selectedTeam = team.target.value;
    this.loadStats();
  }

  private loadStats() {
        var str: string = "";
        if (this.selectedTeam != "")
            str = this.selectedTeam + "-";
        var path: string = "assets/" + str + "Overall" + this.selectedStat + "Performance.json";

        if (this.selectedStat == "Batting")
            this.columns = this.columnsBatting;
        else
            this.columns = this.columnsBowling;

        this.loadJsonDataFromDisk(path);
    }

  onChangeStatType(statType: any) {
    this.selectedStat = statType.target.value;
    this.loadStats();  }

  loadJsonDataFromDisk(fileName: string) {
    this.httpClient.get(fileName).subscribe(data => {
      this.sourceBatting = JSON.parse(JSON.stringify(data));
    })

    

  }
}
