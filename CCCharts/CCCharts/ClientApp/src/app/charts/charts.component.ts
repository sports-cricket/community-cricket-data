import { Component, OnInit } from '@angular/core';
import { Chart }from 'chart.js';

@Component({
  selector: 'app-charts-component',
  templateUrl: './charts.component.html'
})
export class ChartsComponent implements OnInit {
  ngOnInit(): void {

    const ctpLinePP = 'powerPlayLine';
    const powerPlayLine = new Chart(ctpLinePP, {
      type: 'line',
      data: {
        labels: ['1(Schild)', '2(CkBro)', '3(Ekattor)', '4(Warriors)', '5(CtgRoyale)', '6(PiFi)', '7(Infantry)', '8(Sixer)', '9(PiFfi)', '10(OzNSUers)', '11(AB+)', '12(ScorpSpirit)', '13(WPS)', '14(RR)'],
        datasets: [{
          label: '1st power play (data points).',
          data: [9.6, 7.2,4.6,8.6,4.8,6.4,5.6,4.8,2.8,5.6,3.8,3.8,4, 9.4],
          fill: true,
          borderColor: 'green',
          tension: 0.1
        },
          {
            label: '1st power play (cumulative).',
            data: [9.6, 8.4, 7.13, 7.5, 6.96, 6.86, 6.68, 6.4, 6, 5.96, 5.76, 5.6, 5.47, 5.78],
            fill: true,
            borderColor: 'orange',
            tension: 0.1
          }
        ]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });

    const ctpLineInnings = 'inningsLine';
    const inningsLine = new Chart(ctpLineInnings, {
      type: 'line',
      data: {
        labels: ['1(Schild)', '2(CkBro)', '3(Ekattor)', '4(Warriors)', '5(CtgRoyale)', '6(PiFi)', '7(Infantry)', '8(Sixer)', '9(PiFfi)', '10(OzNSUers)', '11(AB+)', '12(ScorpSpirit)', '13(WPS)', '14(RR)'],
        datasets: [{
          label: 'Full innings (data points).',
          data: [9.27, 6.6, 6.25, 8.85, 7.05, 5.9, 9.1, 5.6, 4.2, 5.98, 5.57, 4.69, 4.46, 7.28],
          fill: true,
          borderColor: 'green',
          tension: 0.1
        },
        {
          label: 'Full innings (Cumulative).',
          data: [9.27, 7.93, 7.37, 7.67, 7.6, 7.32, 7.57, 7.32, 6.98, 6.88, 6.75, 6.58, 6.33, 6.42],
          fill: true,
          borderColor: 'orange',
          tension: 0.1
        }
        ]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });

    const ctpLinePPRpw = 'powerPlayLineRpw';
    const powerPlayLineRpw = new Chart(ctpLinePPRpw, {
      type: 'line',
      data: {
        labels: ['1(Schld)', '2(CB)', '3(Ekt)', '4(War)', '5(Ctgr)', '6(PF)', '7(Inf)', '8(Sxr)', '9(PF)', '10(AzNSU)', '11(AB+)', '12(ScrSp)', '13(WPS)', '14(RR)'],
        datasets: [{
          label: '1st power play (data points).',
          data: [48, 18, 23, 21.5, 12, 10.66, 28, 24, 7, 28, 4.75, 9.5, 6.66, 23.5],
          fill: true,
          borderColor: 'green',
          tension: 0.1
        },
        {
          label: '1st power play (cumulative).',
          data: [48, 42, 35.66, 30, 24.85, 20.06, 23.4, 23.45, 20.92, 21.42, 17.72, 16.9, 15.56, 16.2],
          fill: true,
          borderColor: 'orange',
          tension: 0.1
        }
        ]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });

    const ctpLineInningsRpw = 'inningsLineRpw';
    const inningsLineRpw = new Chart(ctpLineInningsRpw, {
      type: 'line',
      data: {
        labels: ['1(Schld)', '2(CB)', '3(Ekt)', '4(War)', '5(Ctgr)', '6(PF)', '7(Inf)', '8(Sxr)', '9(PF)', '10(AzNSU)', '11(AB+)', '12(ScrSp)', '13(WPS)', '14(RR)'],
        datasets: [{
          label: 'Full innings (data points).',
          data: [57.5, 18.85, 17.85, 36, 20.14, 13.11, 26, 12.44, 15, 32, 13,8.6, 14.5,26],
          fill: true,
          borderColor: 'green',
          tension: 0.1
        },
        {
          label: 'Full innings (Cumulative).',
          data: [57.5, 27.44, 23.5, 25.26, 23.88, 21.11, 21.92, 20.25, 19.62, 20.41, 19.38, 18.07, 18.05,18.43],
          fill: true,
          borderColor: 'orange',
          tension: 0.1
        }
        ]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });


    const ctp = 'powerPlay';
    const powerPlay = new Chart(ctp, {
      type: 'bar',
      data: {
        labels: ['Sadhinota Cup 2021', 'BSL 3.0'],
        datasets: [{
          label: 'First power play...',
          barThickness: 100,
          data: [6.45, 4],
          backgroundColor: [
            'green',
            'blue'
          ],
          borderColor: [
            'green',
            'blue'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });

    const cti = 'innings';
    const innings = new Chart(cti, {
      type: 'bar',
      data: {
        labels: ['Sadhinota Cup 2021', 'BSL 3.0'],
        datasets: [{
          label: 'Full Innings...',
          barThickness: 100,
          data: [7.16, 4.95],
          backgroundColor: [
            'green',
            'blue'
          ],
          borderColor: [
            'green',
            'blue'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });

    const ctPpRpw = 'powerPlayRpw';
    const powerPlayRpw = new Chart(ctPpRpw, {
      type: 'bar',
      data: {
        labels: ['Sadhinota Cup 2021', 'BSL 3.0'],
        datasets: [{
          label: 'First power play...',
          barThickness: 100,
          data: [23.45, 8.33],
          backgroundColor: [
            'green',
            'blue'
          ],
          borderColor: [
            'green',
            'blue'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });

    const ctiRpw = 'inningsRpw';
    const inningsRpw = new Chart(ctiRpw, {
      type: 'bar',
      data: {
        labels: ['Sadhinota Cup 2021', 'BSL 3.0'],
        datasets: [{
          label: 'Full Innings...',
          barThickness: 100,
          data: [20.25, 14.48],
          backgroundColor: [
            'green',
            'blue'
          ],
          borderColor: [
            'green',
            'blue'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });

    const ctcpp = 'tcpowerPlayRr';
    const tcPowerPlayRr = new Chart(ctcpp, {
      type: 'bar',
      data: {
        labels: ['Sydney Fighters', 'Power Source', 'Ctg Royale'],
        datasets: [{
          label: 'First power play...',
          barThickness: 100,
          data: [6.45, 5.54, 5.93],
          backgroundColor: [
            'orange',
            'pink',
            'yellow'
          ],
          borderColor: [
            'orange',
            'pink',
            'yellow'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });

    const ctci = 'tcInningsRr';
    const tcInningsRr = new Chart(ctci, {
      type: 'bar',
      data: {
        labels: ['Sydney Fighters', 'Power Source', 'Ctg Royale'],
        datasets: [{
          label: 'Full Innings...',
          barThickness: 100,
          data: [7.82, 7.23, 5.7],
          backgroundColor: [
            'orange',
            'pink',
            'yellow'
          ],
          borderColor: [
            'orange',
            'pink',
            'yellow'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });

    const ctcpprpw = 'tcpowerPlayRpw';
    const tcPowerPlayRpw = new Chart(ctcpprpw, {
      type: 'bar',
      data: {
        labels: ['Sydney Fighters', 'Power Source', 'Ctg Royale'],
        datasets: [{
          label: 'First power play...',
          barThickness: 100,
          data: [28.25, 23.46, 18.18],
          backgroundColor: [
            'orange',
            'pink',
            'yellow'
          ],
          borderColor: [
            'orange',
            'pink',
            'yellow'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });

    const ctcirpw = 'tcInningsRpw';
    const tcInningsRpw = new Chart(ctcirpw, {
      type: 'bar',
      data: {
        labels: ['Sydney Fighters', 'Power Source', 'Ctg Royale'],
        datasets: [{
          label: 'Full Innings...',
          barThickness: 100,
          data: [19.40, 21.30, 13.29],
          backgroundColor: [
            'orange',
            'pink',
            'yellow'
          ],
          borderColor: [
            'orange',
            'pink',
            'yellow'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });

    const ctcppBsl = 'tcpowerPlayRrBsl';
    const tcPowerPlayRrBsl = new Chart(ctcppBsl, {
      type: 'bar',
      data: {
        labels: ['Sydney Fighters', 'Power Source', 'Ctg Royale'],
        datasets: [{
          label: 'First power play...',
          barThickness: 100,
          data: [4, 5.2, 3.8],
          backgroundColor: [
            'orange',
            'pink',
            'yellow'
          ],
          borderColor: [
            'orange',
            'pink',
            'yellow'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });

    const ctciBsl = 'tcInningsRrBsl';
    const tcInningsRrBsl = new Chart(ctciBsl, {
      type: 'bar',
      data: {
        labels: ['Sydney Fighters', 'Power Source', 'Ctg Royale'],
        datasets: [{
          label: 'Full Innings...',
          barThickness: 100,
          data: [4.95, 5.7, 4.43],
          backgroundColor: [
            'orange',
            'pink',
            'yellow'
          ],
          borderColor: [
            'orange',
            'pink',
            'yellow'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });

    const ctcpprpwBsl = 'tcpowerPlayRpwBsl';
    const tcPowerPlayRpwBsl = new Chart(ctcpprpwBsl, {
      type: 'bar',
      data: {
        labels: ['Sydney Fighters', 'Power Source', 'Ctg Royale'],
        datasets: [{
          label: 'First power play...',
          barThickness: 100,
          data: [8.33, 21.66, 15.2],
          backgroundColor: [
            'orange',
            'pink',
            'yellow'
          ],
          borderColor: [
            'orange',
            'pink',
            'yellow'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });

    const ctcirpwBsl = 'tcInningsRpwBsl';
    const tcInningsRpwBsl = new Chart(ctcirpwBsl, {
      type: 'bar',
      data: {
        labels: ['Sydney Fighters', 'Power Source', 'Ctg Royale'],
        datasets: [{
          label: 'Full Innings...',
          barThickness: 100,
          data: [14.48, 21.17, 13],
          backgroundColor: [
            'orange',
            'pink',
            'yellow'
          ],
          borderColor: [
            'orange',
            'pink',
            'yellow'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });


  }
  public currentCount = 0;

  public incrementCounter() {
    this.currentCount++;
  }
}
